unit MainForm;

{*******************************************************}
{                                                       }
{       Special for Auslogic                            }
{                                                       }
{       Copyright (c) 2017 easty                        }
{                                                       }
{*******************************************************}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Controls, Forms,
  Dialogs, ComCtrls, ShellTreeView, StdCtrls, ImagesPreview, ImgList;

type
  TMainFrm = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TreeViewClick(Sender: TObject);
    procedure TreeViewDblClick(Sender: TObject);
  private
    { Private declarations }
    FSheelTreeView: TMyShellTreeView;
    FPrevView: TImagesPreview;
    procedure ShowFolderPreview(APath: String);
  public
    { Public declarations }
  end;

const
  WidgetPadding = 10;

var
  MainFrm: TMainFrm;

implementation

{$R *.dfm}

procedure TMainFrm.FormCreate(Sender: TObject);
begin
  FSheelTreeView := TMyShellTreeView.Create(self);
  FSheelTreeView.Parent := self;
  FSheelTreeView.OnClick := TreeViewClick;
  FSheelTreeView.OnDblClick := TreeViewDblClick;
  FSheelTreeView.RootFolder := SHTV_ROOT_FOLDER_NAME;
  FPrevView := TImagesPreview.Create(self);
  FPrevView.Parent := self;
end;

procedure TMainFrm.FormResize(Sender: TObject);
begin
  FSheelTreeView.Top := WidgetPadding;
  FSheelTreeView.Height := ClientHeight - WidgetPadding * 2;
  FSheelTreeView.Left := WidgetPadding;
  FSheelTreeView.Width := Trunc(ClientWidth / 2) - WidgetPadding * 2;
  FPrevView.Top := WidgetPadding;
  FPrevView.Height := ClientHeight - WidgetPadding * 2;
  FPrevView.Left := Trunc(ClientWidth / 2) + WidgetPadding * 2;
  FPrevView.Width := Trunc(ClientWidth / 2) - WidgetPadding * 3;
end;

procedure TMainFrm.ShowFolderPreview(APath: String);
begin
  FPrevView.Folder := APath;
end;

procedure TMainFrm.TreeViewClick(Sender: TObject);
begin
  ShowFolderPreview(FSheelTreeView.GetPathToView);
end;

procedure TMainFrm.TreeViewDblClick(Sender: TObject);
begin
  ShowFolderPreview(FSheelTreeView.GetPathToView);
end;

end.
