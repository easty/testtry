unit ImagesPreview;

{*******************************************************}
{                                                       }
{       Special for Auslogic                            }
{                                                       }
{       Copyright (c) 2017 easty                        }
{                                                       }
{*******************************************************}

interface

uses
  ComCtrls, Classes, Windows, RunThreadsU, Controls, SyncObjs, SysUtils,
  Graphics, jpeg,
  pngimage, Types;

type

  TLoaderParams = class(TObject)
  private
    Folder: String;
    imgWidth: Integer;
    imgHeight: Integer;
  end;

  TPushedImage = class(TObject)
    Bmp: TBitMap;
    Caption: String;
  public
    constructor Create;
    destructor Destroy; override;
  end;

  TImagesPreview = class(TListView)
  private
    FImages: TImageList;
    FLoader: TRunThread;
    FLock: TCriticalSection;
    FFolder: String;
    procedure TerminateLoader;
    procedure StartLoader(AFolder: String);
    procedure ReceiveImage(APushedImage: Pointer);
  protected
    procedure SetFolder(AFolder: String);
    procedure LoadPreviewsThreaded(AThread: TRunThread);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property Folder: String read FFolder write SetFolder;
  end;

implementation

{ TImagesPreview }

constructor TImagesPreview.Create(AOwner: TComponent);
begin
  inherited;
  FLock := TCriticalSection.Create;
  FLoader := nil;
  FImages := TImageList.Create(self);
  LargeImages := FImages;
  FImages.Width := 100;
  FImages.Height := 100;
end;

destructor TImagesPreview.Destroy;
begin
  { Stop loader job if in progress }
  TerminateLoader;
  if Assigned(FImages) then
  begin
    FImages.Clear;
    FreeAndNil(FImages);
  end;
  if Assigned(FLock) then
  begin
    FreeAndNil(FLock);
  end;
  inherited;
end;

// LoadPreviewsThreaded 27.12.2017 (c) easty
procedure TImagesPreview.LoadPreviewsThreaded(AThread: TRunThread);
var
  FJob: TLoaderParams;
  SR: TSearchRec;
  fPic: TPicture;
  isPicture: Boolean;
  Bmp: TBitMap;
  r: TRect;
  pImage: TPushedImage;
  sExt: String;
begin
  // check if we have data for progress
  if (Assigned(AThread.AData)) then
    FJob := AThread.AData
  else
    exit;
  try
    // check that we can sending data to  main thread (canPush)
    if (FJob.Folder = '') or (not AThread.canPush()) then
      exit;
    r.Left := 0;
    r.Top := 0;
    if FindFirst(FJob.Folder + '\*.*', faAnyFile, SR) = 0 then
    begin
      repeat
        if (AThread.Terminated) then
          Break;
        fPic := TPicture.Create;
        try
          sExt := LowerCase(ExtractFileExt(SR.Name));
          // only jpeg png and bmp according task rules
          if (sExt <> '.jpg') and (sExt <> '.png') and (sExt <> '.bmp') and
            (sExt <> '.jpeg') then
            Continue;
          try
            fPic.LoadFromFile(FJob.Folder + '\' + SR.Name);
            isPicture := true;
          except
            // can`t load image, igniring
            isPicture := false;
          end;
          if isPicture then
          begin
            try
              { image will be free in main thread }
              Bmp := TBitMap.Create;
              Bmp.AlphaFormat := afDefined;
              Bmp.SetSize(FJob.imgWidth, FJob.imgHeight);
              // calculate resize params
              if fPic.Width * Bmp.Height <= fPic.Height * Bmp.Width then
              begin
                r.Bottom := Bmp.Height;
                r.Right := fPic.Width * Bmp.Height div fPic.Height;
              end
              else
              begin
                r.Right := Bmp.Width;
                r.Bottom := fPic.Height * Bmp.Width div fPic.Width;
              end;
              r.Left := 1;
              r.Top := 1;
              r.Bottom := r.Bottom - 1;
              r.Right := r.Right - 1;
              with Bmp do
              begin
                PixelFormat := pf24bit;
                TransparentMode := tmAuto;
                Canvas.CopyMode := cmSrcCopy;
                Canvas.Lock;
                Canvas.StretchDraw(r, fPic.Graphic);
                Canvas.Unlock;
              end;
              // sending image to main thread
              // bmp must dosposed in main thread
              pImage := TPushedImage.Create;
              pImage.Bmp := Bmp;
              pImage.Caption := SR.Name;
              // synchronously push data to list
              AThread.PushData(pImage);
            finally
            end;
          end;
        finally
          FreeAndNil(fPic);
        end;
      until (FindNext(SR) <> 0);
      SysUtils.FindClose(SR);
    end;
  finally
    FreeAndNil(FJob);
  end;
end;

procedure TImagesPreview.ReceiveImage(APushedImage: Pointer);
var
  imgP: TPushedImage;
  FItem: TListItem;
begin
  if not Assigned(APushedImage) then
    exit;
  imgP := APushedImage;
  try
    Items.BeginUpdate;
    FItem := Items.Add;
    FItem.Caption := imgP.Caption;
    FItem.ImageIndex := FImages.Add(imgP.Bmp, nil);
  finally
    FreeAndNil(imgP);
    Items.EndUpdate;
  end;
end;

procedure TImagesPreview.SetFolder(AFolder: String);
begin
  if FFolder = AFolder then
    exit;
  FFolder := AFolder;
  TerminateLoader;
  Items.BeginUpdate;
  try
    Items.Clear;
    FImages.Clear;
  finally
    Items.EndUpdate;
  end;
  StartLoader(AFolder);
end;

procedure TImagesPreview.TerminateLoader;
begin
  FLock.Enter;
  try
    if Assigned(FLoader) then
    begin
      if not FLoader.Terminated then
      begin
        FLoader.Terminate;
        FLoader.WaitFor;
        FLoader.Free;
        FLoader := nil;
      end;
    end;
  finally
    FLock.Leave;
  end;
end;

procedure TImagesPreview.StartLoader(AFolder: String);
var
  FJob: TLoaderParams;
begin
  FLock.Enter;
  try
    if not Assigned(FLoader) then
    begin
      FLoader := TRunThread.Create(true, false);
      FLoader.OnThreadRun := LoadPreviewsThreaded;
      FLoader.OnThreadPushData := ReceiveImage;
      FJob := TLoaderParams.Create;
      FJob.Folder := AFolder;
      FJob.imgWidth := FImages.Width;
      FJob.imgHeight := FImages.Height;
      FLoader.AData := FJob;
      FLoader.Start;
    end;
  finally
    FLock.Leave;
  end;
end;

constructor TPushedImage.Create;
begin
  Bmp := nil;
  inherited Create;
end;

destructor TPushedImage.Destroy;
begin
  if (Assigned(Bmp)) then
    Bmp.Free;
  inherited Destroy;
end;

end.
