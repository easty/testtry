unit ShellTreeView;

{*******************************************************}
{                                                       }
{       Special for Auslogic                            }
{                                                       }
{       Copyright (c) 2017 easty                        }
{                                                       }
{*******************************************************}

interface

uses
  ComCtrls, Classes, SysUtils, Windows, utils;

type
  PFolderInfo = ^TFolderInfo;

  TFolderInfo = class(TObject)
    Name: String;
    RealName: String;
    isRoot: Boolean;
    isVirtual: Boolean;
    isDisk: Boolean;
  public
    constructor Create;
  end;

  TMyShellTreeView = class(TTreeView)
  private
    FRootFolder: String;
    procedure SetRootFolder(val: String);
    procedure GetSubFolders(ANode: TTreeNode; Folder: String; Expand: Boolean);
    procedure GetDrives(ANode: TTreeNode);
    procedure DblClick; override;
    procedure Click; override;
    procedure Delete(Node: TTreeNode); override;
    procedure Added(Node: TTreeNode); override;
    function GetFullPath(ANode: TTreeNode): String;
  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function GetPathToView: String;
    property RootFolder: String read FRootFolder write SetRootFolder;
  published

  end;

const
  SHTV_ROOT_FOLDER_NAME = '��� ���������';

implementation

{ TMyShellTreeView }

procedure TMyShellTreeView.SetRootFolder(val: String);
var
  AData: TFolderInfo;
  ANode: TTreeNode;
begin
  FRootFolder := val;
  self.Items.Clear;
  ANode := self.Items.Add(nil, val);
  AData := TFolderInfo.Create;
  AData.isRoot := true;
  AData.isVirtual := true;
  AData.Name := SHTV_ROOT_FOLDER_NAME;
  ANode.Data := AData;
  GetSubFolders(ANode, FRootFolder, true);
end;

procedure TMyShellTreeView.Added(Node: TTreeNode);
begin
  inherited;
  Node.Data := nil;
end;

procedure TMyShellTreeView.Click;
begin
  inherited;
  if (Assigned(self.Selected)) then
  begin
    if not self.Selected.HasChildren then
      GetSubFolders(self.Selected, self.Selected.Text, false);
  end;
end;

constructor TMyShellTreeView.Create(AOwner: TComponent);
begin
  inherited;
  ReadOnly := true;
end;

procedure TMyShellTreeView.DblClick;
begin
  inherited;
  if (Assigned(self.Selected)) then
  begin
    if not self.Selected.HasChildren then
      GetSubFolders(self.Selected, self.Selected.Text, true);
  end;
end;

procedure TMyShellTreeView.Delete(Node: TTreeNode);
var
  AData: TFolderInfo;
begin
  if (Assigned(Node.Data)) then
  begin
    AData := Node.Data;
    AData.Free;
    Node.Data := nil;
  end;
  inherited;
end;

destructor TMyShellTreeView.Destroy;
begin
  inherited;
end;

function TMyShellTreeView.GetFullPath(ANode: TTreeNode): String;
var
  sPath: String;
  AData: TFolderInfo;
begin
  Result := '';
  sPath := '';
  if not Assigned(ANode) then
    exit;
  repeat
    if Assigned(ANode.Parent) then
    begin
      AData := ANode.Data;
      if Assigned(AData) then
        sPath := AData.RealName + SysUtils.PathDelim + sPath
      else
        sPath := ANode.Text + SysUtils.PathDelim + sPath;
    end;
    ANode := ANode.Parent;
  until (not Assigned(ANode));
  Result := sPath;
end;

function TMyShellTreeView.GetPathToView: String;
var
  AData: TFolderInfo;
begin
  Result := '';
  if not Assigned(Selected) or not Assigned(Selected.Data) then
    exit;
  AData := Selected.Data;
  if not AData.isRoot and not AData.isVirtual then
    Result := GetFullPath(Selected);
end;

procedure TMyShellTreeView.GetDrives(ANode: TTreeNode);
var
  AData: TFolderInfo;
  NNode: TTreeNode;
  cDrives: Cardinal;
  nDrvList: array [0 .. 128] of Char;
  sDrvLatter: PChar;
  sDisk: String;
begin
  cDrives := GetLogicalDriveStrings(SizeOf(nDrvList), nDrvList);
  if cDrives = 0 then
    exit;
  sDrvLatter := nDrvList;
  while sDrvLatter^ <> #0 do
  begin
    sDisk := copy(StrPas(sDrvLatter), 1, 1);
    NNode := Items.AddChild(ANode, GetVolumeLabel(sDisk) + ' (' + sDisk + ')');
    AData := TFolderInfo.Create;
    AData.RealName := sDisk + ':';
    AData.Name := ANode.Text;
    NNode.Data := AData;
    Inc(sDrvLatter, SizeOf(sDrvLatter));
  end;
end;

procedure TMyShellTreeView.GetSubFolders(ANode: TTreeNode; Folder: String;
  Expand: Boolean);
var
  AData: TFolderInfo;
  PData: TFolderInfo;
  NNode: TTreeNode;
  i: Integer;
  SR: TSearchRec;
  sPath: String;
begin
  Items.BeginUpdate;
  try
    if not Assigned(ANode.Data) then
      exit;
    PData := ANode.Data;
    if PData.isRoot then
    begin
      GetDrives(ANode);
      exit;
    end;
    sPath := GetFullPath(ANode);
    if FindFirst(sPath + '*.*', faDirectory, SR) = 0 then
    begin
      repeat
        if ((SR.Attr and faDirectory) = faDirectory) and (SR.Name <> '.') and
          (SR.Name <> '..') then
        begin
          NNode := Items.AddChild(ANode, SR.Name);
          AData := TFolderInfo.Create;
          AData.RealName := SR.Name;
          AData.Name := SR.Name;
          NNode.Data := AData;
        end;
      until FindNext(SR) <> 0;
      FindClose(SR.FindHandle);
    end;
  finally
    Items.EndUpdate;
    if Expand then
      ANode.Expand(false);
  end;
end;

constructor TFolderInfo.Create;
begin
  inherited Create;
  isVirtual := false;
  isRoot := false;
  isDisk := false;
end;

end.
