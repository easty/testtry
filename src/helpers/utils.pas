unit utils;

{*******************************************************}
{                                                       }
{       Special for Auslogic                            }
{                                                       }
{       Copyright (c) 2017 easty                        }
{                                                       }
{*******************************************************}

interface

Uses Windows, SysUtils;

function GetVolumeLabel(DriveName: String): String;

implementation

function GetVolumeLabel(DriveName: String): String;
var
  NotUsed: DWORD;
  VolumeFlags: DWORD;
  VolumeInfo: array [0 .. MAX_PATH] of Char;
  VolumeSerialNumber: DWORD;
  Buf: array [0 .. MAX_PATH] of Char;
begin
  Result := 'NONAME';
  Buf[0] := #0;
  GetVolumeInformation(PChar(DriveName + ':\'), Buf, SizeOf(VolumeInfo),
    @VolumeSerialNumber, NotUsed, VolumeFlags, nil, 0);
  if Buf[0] <> #0 then
    SetString(Result, Buf, StrLen(Buf));
  Result := AnsiUpperCase(Result)
end;

end.
