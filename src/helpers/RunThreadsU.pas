unit RunThreadsU;

{*******************************************************}
{                                                       }
{       Special for Auslogic                            }
{                                                       }
{       Copyright (c) 2017 easty                        }
{                                                       }
{*******************************************************}

interface

uses
  Classes, SyncObjs;

type

  TRunThread = class;

  TRunThreadEvent = procedure(AThread: TRunThread) of object;
  TRunThreadResult = procedure(AResult: Pointer) of object;
  TRunThreadData = procedure(AData: Pointer) of object;

  TRunThread = class(TThread)
  private
  protected
    FOnRunThread: TRunThreadEvent;
    FOnThreadEnd: TRunThreadResult;
    FOnThreadPushData: TRunThreadData;
    procedure Execute; override;
  public
    AData: Pointer;
    function canPush: Boolean;
    procedure Synchronize(Method: TThreadMethod); overload;
    property OnThreadRun: TRunThreadEvent read FOnRunThread write FOnRunThread;
    property OnThreadEnd: TRunThreadResult read FOnThreadEnd write FOnThreadEnd;
    property OnThreadPushData: TRunThreadData read FOnThreadPushData
      write FOnThreadPushData;
    property Terminated;
    procedure PushData(APData: Pointer);
    constructor Create(CreateSuspended: Boolean); overload;
    constructor Create(CreateSuspended, AFreeOnTerminate: Boolean); overload;
    destructor Destroy;
  end;

implementation

{ TRunThread }

constructor TRunThread.Create(CreateSuspended: Boolean);
begin
  AData := nil;
  FOnThreadPushData := nil;
  FOnRunThread := nil;
  FOnRunThread := nil;
  inherited Create(CreateSuspended);
end;

function TRunThread.canPush: Boolean;
begin
  if Assigned(FOnThreadPushData) then
    Result := true
  else
    Result := false;
end;

constructor TRunThread.Create(CreateSuspended, AFreeOnTerminate: Boolean);
begin
  Create(CreateSuspended);
  FreeOnTerminate := AFreeOnTerminate;
end;

destructor TRunThread.Destroy;
begin
  inherited Destroy;
end;

procedure TRunThread.Execute;
begin
  if Assigned(FOnRunThread) then
    FOnRunThread(self);
end;

procedure TRunThread.PushData(APData: Pointer);
begin
  Synchronize( procedure begin if Assigned(FOnThreadPushData)
  then FOnThreadPushData(APData); end);
end;

procedure TRunThread.Synchronize(Method: TThreadMethod);
begin
  inherited Synchronize(Method);
end;

end.
