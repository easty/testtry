program testtry;

{*******************************************************}
{                                                       }
{       Special for Auslogic                            }
{                                                       }
{       Copyright (c) 2017 easty                        }
{                                                       }
{*******************************************************}

uses
  Forms,
  Windows,
  MainForm in 'src\forms\MainForm.pas' {MainFrm},
  ShellTreeView in 'src\widgets\ShellTreeView.pas',
  RunThreadsU in 'src\helpers\RunThreadsU.pas',
  utils in 'src\helpers\utils.pas',
  ImagesPreview in 'src\widgets\ImagesPreview.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainFrm, MainFrm);
  Application.Run;
end.
